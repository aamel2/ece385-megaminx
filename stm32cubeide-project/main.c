/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */

#include "math.h"
#include "stdio.h"
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim14;

/* USER CODE BEGIN PV */
CAN_TxHeaderTypeDef   TxHeader;
uint8_t               TxData[8];
uint32_t              TxMailbox;

CAN_RxHeaderTypeDef   RxHeader;
uint8_t               RxData[8];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM14_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t id = 10;

uint16_t cl, ch;
float duty;
float speed;
float pos;

float KP = 0.5;
float KI = 0.05;
float KD = 0.2;
float max_err = 0.04;
float setpoint;
float err;
float prev_err;
float integral;

int sensor;
int led_on;
int curr_ch;
int status;
int doneMoving;
int notifyMove;
int readCycle;
int moveCounter;

float rel_buf[3] = {1, 0, 0};
uint8_t buf[8];
uint8_t control[6] = {0x40, 0x4b, 0x00, 0x12, 0x02, 0x02};

uint8_t sensordata[5][10][3];
uint8_t colors[10][3];
//                     0  1  2  3  4  5  6  7  8  9
uint8_t working[10] = {1, 0, 0, 1, 0, 0, 0, 1, 0, 1};
uint8_t workingE = 3;
uint8_t workingC = 1;


float dist(float a, float b) {
    float d = fabs(a - b);
    return d > 0.5 ? 1 - d : d;
}

int direction(float p, float s) {
    float d = dist(p, s);
    float del = fabs(p - s);
    int ret = 0;
    if (p < s) {
        if(d <= 0.5) {ret = -1;}
        else {ret = 1;}
    } else {
    	if(d <= 0.5) {ret = 1;}
    	else {ret = -1;}
    }

    if(del > 0.5) {return ret * -1;}
    else{return ret;}
}

/*float invsqrt(float number)
{
  long i;
  float x2, y;
  const float threehalfs = 1.5F;

  x2 = number * 0.5F;
  y  = number;
  i  = * ( long * ) &y;                       // evil floating point bit level hacking
  i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
  y  = * ( float * ) &i;
  y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
  // y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

  return y;
}*/

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
    if (htim->Instance == TIM2) {
        cl = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
        ch = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);

        duty = 100 * ch / cl;
        //duty = 100 * ch * invsqrt(cl) * invsqrt(cl); //saves 400ish bytes

    }
}

float pid(float error, float prev_error, float integral) {
	// PID terms
    float proportional_term = KP * error;
    float integral_term = KI * integral;
    float derivative_term = KD * (error - prev_error);

    // Calculate the output velocity
    float velocity = proportional_term + integral_term + derivative_term;

    // Clamp the output velocity to the range [-1, 1]
    if (velocity > 1.0) {
        velocity = 1.0;
    } else if (velocity < -1.0) {
        velocity = -1.0;
    }

    return velocity;
}

void initSensor (int ch){
	uint8_t mux[1] = {0x08 + ch/2};
	uint8_t cs = 0x70 + (ch%2)*2;

    HAL_I2C_Master_Transmit(&hi2c1, 0xe0, mux, 1, HAL_MAX_DELAY);
    HAL_I2C_Master_Transmit(&hi2c1, cs, control, 6, HAL_MAX_DELAY);
}

void readSensor (int ch){
	uint8_t mux[1] = {0x08 + ch/2};
	uint8_t cs = 0x70 + (ch%2)*2;
	uint8_t regaddr[1] = {0x50};

	HAL_I2C_Master_Transmit(&hi2c1, 0xe0, mux, 1, HAL_MAX_DELAY);
	HAL_I2C_Master_Transmit(&hi2c1, cs, regaddr, 1, HAL_MAX_DELAY);
	HAL_I2C_Master_Receive(&hi2c1, cs, buf, 8, HAL_MAX_DELAY);
	rel_buf[1] = (float) buf[3]/buf[1];
	rel_buf[2] = (float) buf[5]/buf[1];

}

void setLED (int on){
	if(on) {  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);}
	else   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData);
	if(RxHeader.StdId > 16){
		if(RxData[0]  == id) {
			status = RxData[1];
			if((status & 0x03) == 0x00){ setpoint -= 0.2; }
			if((status & 0x03) == 0x01){ setpoint -= 0.4; }
			if((status & 0x03) == 0x02){ setpoint += 0.2; }
			if((status & 0x03) == 0x03){ setpoint += 0.4; }
			if(status & 4) {
				readCycle = 1;
				led_on = 1;

			}
		}
	}
}

void setStatusLED(){
	if(status & 0x80) {HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);}

	if(status & 0x40) {HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);}

	if(status & 0x20) {HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	int sw4 = 1;
	int sw5 = 1;
	int led_state = 1;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  MX_TIM2_Init();
  MX_TIM14_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);


  //HAL_I2C_Master_Transmit(&hi2c1, 0xe0, muxaddr, 1, HAL_MAX_DELAY);
  //HAL_I2C_Master_Transmit(&hi2c1, 0x70, control, 4, HAL_MAX_DELAY);

  for(int i = 0; i < 10; i++) {initSensor(i);}

  for(int i = 0; i < 10; i++) {initSensor(i);}


  TxHeader.IDE = CAN_ID_STD;
  TxHeader.StdId = 0x000 + id;
  TxHeader.RTR = CAN_RTR_DATA;
  TxHeader.DLC = 6;

  TxData[0] = 0x01;

   HAL_CAN_Start(&hcan);
   HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

   pos = (duty - 2.70)*(0.0105932203);
   setpoint = pos;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while(1) {
	  if(speed > 1){ speed = 1; }
	  if(speed < -1) { speed = -1; }

	  if(setpoint > 1) {setpoint -= 1;}
  	  if(setpoint < 0) {setpoint += 1;}

	  TIM14->CCR1 = 75 + (15 * speed);
	  pos = (duty - 2.70)*(0.0105932203);

	  /*if(dist(pos, setpoint) > 0.1) {speed = dist(pos, setpoint)*)*2;}
	  else if(dist(pos, setpoint) > 0.03) {speed = 0.2*direction(pos,setpoint);}
	  else {speed = 0;}*/

	  prev_err = err;
	  integral += err;

	  err = dist(pos, setpoint) * direction(pos, setpoint);

	  if((err*err < max_err*max_err) && (prev_err*prev_err < max_err*max_err)) {
		  integral = 0;
		  doneMoving = 1;
	  }
	  else {
		  doneMoving = 0;
		  notifyMove = 0;
	  }

	  if(doneMoving  && !notifyMove && !readCycle){
		  HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox);
		  notifyMove = 1;
	  }


	  speed = pid(err, prev_err, integral);

	  if(HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_3) && sw5){
	      setpoint += 0.2;
		  sw5 = 0;
	  }

	  if(HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_2) && sw4){
	      setpoint -= 0.2;
	      sw4 = 0;
	  }

	  if(!HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_3)) { sw5 = 1; }
	  if(!HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_2)) { sw4 = 1; }

	  if(HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_1)) { setpoint += 0.002; }
	  if(HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_0)) { setpoint -= 0.002; }

	  if(HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_0) && HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_1) && led_state) {
		  led_on = !led_on;
		  led_state = 0;
	  }
	  else if(HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_0) && HAL_GPIO_ReadPin (GPIOA, GPIO_PIN_1) && !led_state) {
	  		  led_state = 0;
	  }
	  else{ led_state = 1; }

	  readSensor(sensor);
	  setLED(led_on);
	  setStatusLED();

  	  //HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox);

  	  if(readCycle) {
  		  if(doneMoving){
  			  for(int i = 0; i < 10; i++){
  				  if(working[i]) {
						readSensor(i);
						sensordata[moveCounter][i][0] = buf[1];
						sensordata[moveCounter][i][1] = buf[3];
						sensordata[moveCounter][i][2] = buf[5];
  				  }
  			  }
  			  setpoint += 0.2;
  			  moveCounter += 1;
  			  if(moveCounter == 5) {
  				  readCycle = 0;
  				  moveCounter = 0;
  				  for(int i = 0; i < 3; i++){
					  colors[0][i] = sensordata[0][4][i] +
									 sensordata[1][2][i] +
									 sensordata[2][8][i] +
									 sensordata[3][0][i] +
									 sensordata[4][6][i];
					  colors[1][i] = sensordata[0][3][i] +
									 sensordata[1][9][i] +
									 sensordata[2][1][i] +
									 sensordata[3][7][i] +
									 sensordata[4][5][i];
					  colors[2][i] = sensordata[0][2][i] +
									 sensordata[1][8][i] +
									 sensordata[2][0][i] +
									 sensordata[3][6][i] +
									 sensordata[4][4][i];
					  colors[3][i] = sensordata[0][9][i] +
									 sensordata[1][1][i] +
									 sensordata[2][7][i] +
									 sensordata[3][5][i] +
									 sensordata[4][3][i];
					  colors[4][i] = sensordata[0][8][i] +
									 sensordata[1][0][i] +
									 sensordata[2][6][i] +
									 sensordata[3][4][i] +
									 sensordata[4][2][i];
					  colors[5][i] = sensordata[0][1][i] +
									 sensordata[1][7][i] +
									 sensordata[2][5][i] +
									 sensordata[3][3][i] +
									 sensordata[4][9][i];
					  colors[6][i] = sensordata[0][0][i] +
									 sensordata[1][6][i] +
									 sensordata[2][4][i] +
									 sensordata[3][2][i] +
									 sensordata[4][8][i];
					  colors[7][i] = sensordata[0][7][i] +
									 sensordata[1][5][i] +
									 sensordata[2][3][i] +
									 sensordata[3][9][i] +
									 sensordata[4][1][i];
					  colors[8][i] = sensordata[0][6][i] +
									 sensordata[1][4][i] +
									 sensordata[2][2][i] +
									 sensordata[3][8][i] +
									 sensordata[4][0][i];
					  colors[9][i] = sensordata[0][5][i] +
									 sensordata[1][3][i] +
									 sensordata[2][9][i] +
									 sensordata[3][1][i] +
									 sensordata[4][7][i];

					  for(int j = 0; j < 10; j++){
						  if(j%2) {colors[j][i] = colors[j][i]/workingE;}
						  else {colors[j][i] = colors[j][i]/workingC;}
					  }
  				  }
  				  for(int i = 0; i < 5; i++){
  					  TxData[0] = colors[i*2][0];
  					  TxData[1] = colors[i*2][1];
  					  TxData[2] = colors[i*2][2];
  					  TxData[3] = colors[i*2 + 1][0];
  					  TxData[4] = colors[i*2 + 1][1];
  					  TxData[5] = colors[i*2 + 1][2];
  					  HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox);
  					  HAL_Delay(50);
  				  }
  				  led_on = 0;
  			  }
  		  }
  	  }
  	  HAL_Delay(20);

  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN_Init(void)
{

  /* USER CODE BEGIN CAN_Init 0 */

  /* USER CODE END CAN_Init 0 */

  /* USER CODE BEGIN CAN_Init 1 */

  /* USER CODE END CAN_Init 1 */
  hcan.Instance = CAN;
  hcan.Init.Prescaler = 4;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_2TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_1TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN_Init 2 */
  CAN_FilterTypeDef sf;
  sf.FilterMaskIdHigh = 0x0000;
  sf.FilterMaskIdLow = 0x0000;
  sf.FilterFIFOAssignment = CAN_FILTER_FIFO0;
  sf.FilterBank = 0;
  sf.FilterMode = CAN_FILTERMODE_IDMASK;
  sf.FilterScale = CAN_FILTERSCALE_32BIT;
  sf.FilterActivation = CAN_FILTER_ENABLE;
  HAL_CAN_ConfigFilter(&hcan, &sf);
  HAL_CAN_RegisterCallback(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING, HAL_CAN_RxFifo0MsgPendingCallback);

  /* USER CODE END CAN_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_RESET;
  sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
  sSlaveConfig.TriggerPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sSlaveConfig.TriggerPrescaler = TIM_ICPSC_DIV1;
  sSlaveConfig.TriggerFilter = 0;
  if (HAL_TIM_SlaveConfigSynchro(&htim2, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */
  HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_1); // Primary channel - rising edge
  HAL_TIM_IC_Start(&htim2, TIM_CHANNEL_2);    // Secondary channel - falling edge
  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 159;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 999;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim14, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */
  TIM14->CCR1 = 75;
  HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
  /* USER CODE END TIM14_Init 2 */
  HAL_TIM_MspPostInit(&htim14);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_Pin|GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED_Pin PB1 */
  GPIO_InitStruct.Pin = LED_Pin|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SW2_Pin SW3_Pin SW4_Pin SW5_Pin */
  GPIO_InitStruct.Pin = SW2_Pin|SW3_Pin|SW4_Pin|SW5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA6 PA7 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

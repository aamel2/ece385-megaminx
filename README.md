# ece385-megaminx

This is the repository for Anthony Amella and Emily Liu's ECE395 project for Spring 2024.


/kicad contains the KiCAD PCB and Schematic

/stm32cubeide-project contains the main.c and zipped project folder for the MCU

/solver contains the zipped Python code for the puzzle solver. 